package generalmanager;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

public class TitleScreenController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button loadGame;

    @FXML
    private Button newGame;

    @FXML
    void createNewGame(ActionEvent event) throws IOException 
    {
        Parent menuParent = FXMLLoader.load(getClass().getResource("TitleMenu.fxml"));
        Scene menuScene = new Scene(menuParent);
        
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        window.setScene(menuScene);
        window.show();
    }

      @FXML
    void initialize() {
        assert loadGame != null : "fx:id=\"loadGame\" was not injected: check your FXML file 'TitleScreen.fxml'.";
        assert newGame != null : "fx:id=\"newGame\" was not injected: check your FXML file 'TitleScreen.fxml'.";

    }
}

/* void initialize() {
        assert newGame != null : "fx:id=\"newGame\" was not injected: check your FXML file 'TitleScreen.fxml'.";
        assert loadGame != null : "fx:id=\"loadGame\" was not injected: check your FXML file 'TitleScreen.fxml'.";

    }

*/