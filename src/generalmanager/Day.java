/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package generalmanager;

/**
 *
 * @author jtbum
 */
public class Day 
{
    private String month;
    private int dayofWeek;  //num 1-7
    private int date;
    private Event event;    //if event is taking place
    
    public Day()
    {
        
    }
    
    public Day(String month, int dayofWeek, int date)
    {
        this.month = month;
        this.dayofWeek = dayofWeek;
        this.date = date;
    }
    
    public String getMonth()
    {
        return month;
    }
    
    public int getDay()
    {
        return dayofWeek;
    }
    
    public int getDate()
    {
        return date;
    }
}
