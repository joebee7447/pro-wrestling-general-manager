/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package generalmanager;

import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 *
 * @author jtbum
 */
public class Match 
{
    private int matchType; /* 1 = one on one, 2 = hardcore one on one, 3 = two on two, 4 = hardcore two on two
                                5 = three on three, 6 = hardcore three on three, 7 = triple threat
                                8 = fatal 4 way, 9 = six pack
                            */
    private int numParticipants;
    private int numTeams;
    private boolean isForWorld;
    private boolean isForInter;
    private boolean isForTag;
    private boolean isForWomens;
    private boolean isHardcore = false;
    private double rating = 0;
    private int winner;
    private List<Wrestler> participants;
    
    public Match()
    {
        
    }
    
    public Match(int matchType)
    {
        if(matchType == 1)
        {
            numParticipants = 2;
            numTeams = 2;
        }
        else if(matchType == 2)
        {
            numParticipants = 2;
            numTeams = 2;
            isHardcore = true;
        }
        else if(matchType == 3)
        {
            numParticipants = 4;
            numTeams = 2;
        }
        else if(matchType == 4)
        {
            numParticipants = 4;
            numTeams = 2;
            isHardcore = true;
        }
        else if(matchType == 5)
        {
            numParticipants = 6;
            numTeams = 2;
        }
        else if(matchType == 6)
        {
            numParticipants = 6;
            numTeams = 2;
            isHardcore = true;
        }
        else if(matchType == 7)
        {
            numParticipants = 3;
            numTeams = 3;    
        }
        else if(matchType == 8)
        {
            numParticipants = 4;
            numTeams = 4;
        }
        else if(matchType == 9)
        {
            numParticipants = 6;
            numTeams = 6;
        }
        else
        {
            System.out.println("should not happen");
        }
    }
    
    public void isForWorld()
    {
        isForWorld  = true;
        isForInter  = false;
        isForTag    = false;
        isForWomens = false;
    }
    
    public void isForInter()
    {
        isForWorld  = false;
        isForInter  = true;
        isForTag    = false;
        isForWomens = false;
    }
    
    public void isForTag()
    {
        isForWorld  = false;
        isForInter  = false;
        isForTag    = true;
        isForWomens = false;
    }
    
    public void isForWomens()
    {
        isForWorld  = false;
        isForInter  = false;
        isForTag    = false;
        isForWomens = true;
    }
    
    public boolean confirmMatch(List<Wrestler> wrestlers)
    {
        if(wrestlers.size() != numParticipants)
        {
            return false;
        }
        
        
        if(isForWorld == true)
        {
            boolean isChamp = false;
            if(matchType >= 3 && matchType <= 6)    //singles title cannot be defended in tags
            {
                return false;
            }
            
            for(int i = 0; i < numParticipants; i++)
            {
                if(wrestlers.get(i).getIsWorldChamp() == true)
                {
                    isChamp = true;
                }
            }
            
            if(isChamp == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        
        if(isForInter == true)
        {
            boolean isChamp = false;
            if(matchType >= 3 && matchType <= 6)    //singles title cannot be defended in tags
            {
                return false;
            }
            
            for(int i = 0; i < numParticipants; i++)
            {
                if(wrestlers.get(i).getIsInterChamp() == true)
                {
                    isChamp = true;
                }
            }
            
            if(isChamp == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        
        if(isForWomens == true)
        {
            boolean isChamp = false;
            if(matchType >= 3 && matchType <= 6)    //singles title cannot be defended in tags
            {
                return false;
            }
            
            for(int i = 0; i < numParticipants; i++)
            {
                if(wrestlers.get(i).getIsWomensChamp() == true)
                {
                    isChamp = true;
                }
            }
            
            if(isChamp == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        
        if(isForTag == true)
        {
            boolean isChamp = false;
            if(matchType != 3 || matchType != 4)    //tag title cannot be defended in singles or 6 man
            {
                return false;
            }
            
            if(wrestlers.get(0).getIsTagChamp() == true && wrestlers.get(1).getIsTagChamp() == true )
            {
                isChamp = true;
            }
            else if(wrestlers.get(2).getIsTagChamp() == true && wrestlers.get(3).getIsTagChamp() == true )
            {
                isChamp = true;
            }
            
            if(isChamp == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        
        if(matchType == 3 || matchType == 4)
        {
            Wrestler w1 = new Wrestler();
            Wrestler w2 = new Wrestler();
            w1.setPopularity(wrestlers.get(0).getPopularity() + wrestlers.get(1).getPopularity());
            w1.setStrength(wrestlers.get(0).getStrength() + wrestlers.get(1).getStrength());
            w1.setAgility(wrestlers.get(0).getAgility() + wrestlers.get(1).getAgility());
            w1.setTechnical(wrestlers.get(0).getTechnical() + wrestlers.get(1).getTechnical());
            
            w2.setPopularity(wrestlers.get(2).getPopularity() + wrestlers.get(3).getPopularity());
            w2.setStrength(wrestlers.get(2).getStrength() + wrestlers.get(3).getStrength());
            w2.setAgility(wrestlers.get(2).getAgility() + wrestlers.get(3).getAgility());
            w2.setTechnical(wrestlers.get(2).getTechnical() + wrestlers.get(3).getTechnical());
            
            participants.add(w1);
            participants.add(w2);
        }
        else if(matchType == 5 || matchType == 6)
        {
            Wrestler w1 = new Wrestler();
            Wrestler w2 = new Wrestler();
            w1.setPopularity(wrestlers.get(0).getPopularity() + wrestlers.get(1).getPopularity() + wrestlers.get(2).getPopularity());
            w1.setStrength(wrestlers.get(0).getStrength() + wrestlers.get(1).getStrength() + wrestlers.get(2).getStrength());
            w1.setAgility(wrestlers.get(0).getAgility() + wrestlers.get(1).getAgility() + wrestlers.get(2).getAgility());
            w1.setTechnical(wrestlers.get(0).getTechnical() + wrestlers.get(1).getTechnical() + wrestlers.get(2).getAgility());
            
            w2.setPopularity(wrestlers.get(5).getPopularity() + wrestlers.get(3).getPopularity() + wrestlers.get(4).getPopularity());
            w2.setStrength(wrestlers.get(5).getStrength() + wrestlers.get(3).getStrength() + wrestlers.get(4).getStrength());
            w2.setAgility(wrestlers.get(5).getAgility() + wrestlers.get(3).getAgility() + wrestlers.get(4).getAgility());
            w2.setTechnical(wrestlers.get(5).getTechnical() + wrestlers.get(3).getTechnical() + wrestlers.get(4).getTechnical());
            
            participants.add(w1);
            participants.add(w2);
        }
        Collections.copy(participants, wrestlers);
        return true;
    }
    
    public void chooseWinner(int index)
    {
        winner = index;
    }
    
    public void simulateMatch()
    {
        if(matchType >= 1 && matchType <= 6)
        {
            double w1 = 0.5, w2 = 0.5;
            if(participants.get(0).getStrength() > participants.get(1).getAgility())
            {
                w1 += 0.1;
                w2 -= 0.1;
            }
            else if(participants.get(1).getStrength() > participants.get(0).getAgility())
            {
                w1 -= 0.1;
                w2 += 0.1;
            }
            
            if(participants.get(0).getAgility() > participants.get(1).getTechnical())
            {
                w1 += 0.1;
                w2 -= 0.1;
            }
            else if(participants.get(1).getAgility() > participants.get(0).getTechnical())
            {
                w1 -= 0.1;
                w2 += 0.1;
            }
            
            if(participants.get(0).getTechnical() > participants.get(1).getStrength())
            {
                w1 += 0.1;
                w2 -= 0.1;
            }
            else if(participants.get(1).getTechnical() > participants.get(0).getStrength())
            {
                w1 -= 0.1;
                w2 += 0.1;
            }
            
            Random rand = new Random();
            double winVal = rand.nextFloat();
            
            if(winVal <= w1)
            {
                winner = 0;
            }
            else
            {
                winner = 1;
            }
        }
        else if(matchType == 7)
        {
            Random rand = new Random();
            double winVal = rand.nextFloat();
            if(winVal < 0.33333333)
            {
                winner = 0;
                        //participants.get(0);
            }
            else if(winVal > 0.33333333 && winVal < 0.66666666)
            {
                winner  = 1;
            }
            else
            {
                winner = 2;
            }
        }
        else if(matchType == 8)
        {
            Random rand = new Random();
            double winVal = rand.nextFloat();
            if(winVal <= 0.25)
            {
                winner = 0;
            }
            else if(winVal > 0.25 && winVal <= 0.5)
            {
                winner = 1;
            }
            else if(winVal > 0.5 && winVal <= 0.75)
            {
                winner = 2;
            }
            else
            {
                winner = 3;
            }
        }
        else if(matchType == 9)
        {
            Random rand = new Random();
            int winVal = rand.nextInt(6);
            System.out.println("6 MAN WINNER: " + winVal);
            
            if(winVal == 0)
            {
                winner = 0;
            }
            else if(winVal == 1)
            {
                winner = 1;
            }
            else if(winVal == 2)
            {
                winner = 2;
            }
            else if(winVal == 3)
            {
                winner = 3;
            }
            else if(winVal == 4)
            {
                winner = 4;
            }
            else if(winVal == 5)
            {
                winner = 5;
            }
            else
            {
                System.out.println("SHOULD NOT HAPPEN");
            }
        }
        else
        {
            System.out.println("Should never happen");
        }
    }
    
    public Wrestler postMatch(Wrestler w)
    {
        Random rand = new Random();
        w.setDurability(w.getDurability() - rand.nextInt(3));
        if(isHardcore == true)
        {
            w.setDurability(w.getDurability() - rand.nextInt(3));
        }
        
        return w;
    }
    
    public boolean checkForInjury(Wrestler w)
    {
        Random rand = new Random();
        if(w.getDurability() < rand.nextInt(100))
        {
            return true;
        }
        return false;
    }
    public double rateMatch()
    {
        int popSum = 0;
        for(int i = 0; i < participants.size(); i++)
        {
            popSum += participants.get(i).getPopularity();
            popSum += participants.get(i).getOverall();
        }
        
        double avg = popSum / (participants.size() * 2);
        
        if(avg <= 10)
        {
            rating += 0.5;
        }
        else if(avg > 10 && avg <= 20)
        {
            rating += 1.0;
        }
        else if(avg > 20 && avg <= 30)
        {
            rating += 1.5;
        }
        else if(avg > 30 && avg <= 40)
        {
            rating += 2.0;
        }
        else if(avg > 40 && avg <= 50)
        {
            rating += 2.5;
        }
        else if(avg > 50 && avg <= 60)
        {
            rating += 3.0;
        }
        else if(avg > 60 && avg <= 70)
        {
            rating += 3.5;
        }
        else if(avg > 70 && avg <= 80)
        {
            rating += 4.0;
        }
        else if(avg > 80 && avg <= 90)
        {
            rating += 4.5;
        }
        else
        {
            rating += 5.0;
        }
        
        if(isHardcore == true)
        {
            rating += 0.5;
        }
        
        if(isForWorld == true || isForInter == true ||
                isForTag == true || isForWomens == true)
        {
            rating += 0.5;
        }
        
        return rating;
    }
}
