package generalmanager;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

public class TitleMenuController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button draftButton;

    @FXML
    private Button editRosterButton;

    @FXML
    private Button editBrandsButton;

    @FXML
    void editBrands(ActionEvent event) {

    }

    @FXML
    void editRoster(ActionEvent event) throws IOException {

        Parent menuParent = FXMLLoader.load(getClass().getResource("EditAvailableRoster.fxml"));
        Scene menuScene = new Scene(menuParent);
        
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        window.setScene(menuScene);
        window.show();
    }

    @FXML
    void goToDraft(ActionEvent event) {

    }

    @FXML
    void initialize() {
        assert draftButton != null : "fx:id=\"draftButton\" was not injected: check your FXML file 'TitleMenu.fxml'.";
        assert editRosterButton != null : "fx:id=\"editRosterButton\" was not injected: check your FXML file 'TitleMenu.fxml'.";
        assert editBrandsButton != null : "fx:id=\"editBrandsButton\" was not injected: check your FXML file 'TitleMenu.fxml'.";

    }
}
