/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package generalmanager;

/**
 *
 * @author jtbum
 */
public class Email 
{
    private String subjectLine;
    private String contents;
    private String from;
    
    public Email(String subjectLine, String contents, String from)
    {
        this.subjectLine = subjectLine;
        this.contents = contents;
        this.from = from;
    }
}
