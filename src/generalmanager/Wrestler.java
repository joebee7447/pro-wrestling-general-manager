/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package generalmanager;

/**
 *
 * @author jtbum
 */
public class Wrestler 
{
    private String name;
    private int popularity;     //determines most of match rating
    private int strength;       //strength beats agility
    private int technical;      //technical beats strength
    private int agility;        //agility beats technical
    private int hardcore;       //improved ratings in hardcore matches
    private int durability;     //risk of injury, increases and lowers regularly
    private int overall;        //mean of strength, technical, and agility
    private boolean isFemale;   //can compete for womens title only
    private boolean isWorldChamp;   //used for creating title matches
    private boolean isInterChamp;
    private boolean isTagChamp;
    private boolean isWomensChamp;
    private int numWeeksSigned;
    private int injuryLength;
    
    public Wrestler()
    {
        
    }
    
    public Wrestler(String name, int popularity, int strength, int agility, 
            int technical, int hardcore, int durability, boolean isFemale)
    {
        this.name = name;
        this.popularity = popularity;
        this.strength = strength;
        this.agility = agility;
        this.technical = technical;
        this.hardcore = hardcore;
        this.durability = durability;
        this.isFemale = isFemale;
        
        calculateOverall();
    }
    
    public void calculateOverall()
    {
        overall = (strength + agility + technical) / 3;
    }
    
    public int getOverall()
    {
        return overall;
    }
    
    public void setName(String s)
    {
        name = s;
    }
    
    public String getName()
    {
        return name;
    }
    
    public void setPopularity(int h)
    {
        popularity = h;
    }
    
    public int getPopularity()
    {
        return popularity;
    }
    
    public void setStrength(int h)
    {
        strength = h;
        calculateOverall();
    }
    
    public int getStrength()
    {
        return strength;
    }
    
    public void setAgility(int h)
    {
        agility = h;
        calculateOverall();
    }
    
    public int getAgility()
    {
        return agility;
    }
    
    public void setTechnical(int h)
    {
        technical = h;
        calculateOverall();
    }
    
    public int getTechnical()
    {
        return technical;
    }
    
    public void setHardcore(int h)
    {
        hardcore = h;
    }
    
    public int getHardcore()
    {
        return hardcore;
    }
    
    public void setDurability(int d)
    {
        durability = d;
    }
    
    public int getDurability()
    {
        return durability;
    }
    
    public void setIsFemale(boolean f)
    {
        isFemale = f;
    }
    
    public boolean getIsFemale()
    {
        return isFemale;
    }
    
    public boolean getIsWorldChamp()
    {
        return isWorldChamp;
    }
    
    public boolean getIsInterChamp()
    {
        return isInterChamp;
    }
    
    public boolean getIsTagChamp()
    {
        return isTagChamp;
    }
    
    public boolean getIsWomensChamp()
    {
        return isWomensChamp;
    }
    
    public void setWorldChamp(boolean c)
    {
        isWorldChamp = c;
    }
    
    public void setInterChamp(boolean c)
    {
        isInterChamp = c;
    }
    
    public void setTagChamp(boolean c)
    {
        isTagChamp = c;
    }
    
    public void setWomensChamp(boolean c)
    {
        isWomensChamp = c;
    }
}
