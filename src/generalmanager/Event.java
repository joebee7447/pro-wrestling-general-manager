/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package generalmanager;

import java.util.List;

/**
 *
 * @author jtbum
 */
public class Event 
{
    private int brand;  //num 1, 2, or 3 for ppv
    private int numMatches; //num of matches taking place at event
    private int numPromos;  //num promos
    private double rating;  //overall rating of show
    private List<Match> matches;
    private List<Promo> promos;
    
    public Event()
    {
        
    }
    
    public Event(int brand)
    {
        this.brand = brand;
        if(brand == 3)
        {
            numMatches = 10;
            numPromos = 0;
        }
        else
        {
            numMatches = 5;
            numPromos = 2;
        }
    }
    
    public double getRating()
    {
        double sum = 0;
        for(int i = 0; i < matches.size(); i++)
        {
            sum += matches.get(i).rateMatch();
        }
        
        return (sum / matches.size());
    }
}
