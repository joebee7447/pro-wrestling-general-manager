/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package generalmanager;

import java.util.List;

/**
 *
 * @author jtbum
 */
public class Calender 
{
    private List<Day> calender;
    private final int DAYS_IN_APRIL = 30;
    private final int DAYS_IN_MAY = 31;
    private final int DAYS_IN_JUNE = 30;
    private final int DAYS_IN_JULY = 31;
    private final int DAYS_IN_AUGUST = 31;
    private final int DAYS_IN_SEPTEMBER = 30;
    private final int DAYS_IN_OCTOBER = 31;
    private final int DAYS_IN_NOVEMBER = 30;
    private final int DAYS_IN_DECEMBER = 31;
    private final int DAYS_IN_JANUARY = 31;
    private final int DAYS_IN_FEBRUARY = 28;
    private final int DAYS_IN_MARCH = 31;
    
    public Calender()
    {
        int day = 2;
        
        for(int i = 0; i < DAYS_IN_APRIL; i++)
        {
            calender.add(new Day("April", day, i + 1));
            day++;
            if(day > 7)
            {
                day = 1;
            }
        }
        
        for(int i = 0; i < DAYS_IN_MAY; i++)
        {
            calender.add(new Day("May", day, i + 1));
            day++;
            if(day > 7)
            {
                day = 1;
            }
        }
        
        for(int i = 0; i < DAYS_IN_JUNE; i++)
        {
            calender.add(new Day("June", day, i + 1));
            day++;
            if(day > 7)
            {
                day = 1;
            }
        }
        
        for(int i = 0; i < DAYS_IN_JULY; i++)
        {
            calender.add(new Day("July", day, i + 1));
            day++;
            if(day > 7)
            {
                day = 1;
            }
        }
        
        for(int i = 0; i < DAYS_IN_AUGUST; i++)
        {
            calender.add(new Day("August", day, i + 1));
            day++;
            if(day > 7)
            {
                day = 1;
            }
        }
        
        for(int i = 0; i < DAYS_IN_SEPTEMBER; i++)
        {
            calender.add(new Day("September", day, i + 1));
            day++;
            if(day > 7)
            {
                day = 1;
            }
        }
        
        for(int i = 0; i < DAYS_IN_OCTOBER; i++)
        {
            calender.add(new Day("October", day, i + 1));
            day++;
            if(day > 7)
            {
                day = 1;
            }
        }
        
        for(int i = 0; i < DAYS_IN_NOVEMBER; i++)
        {
            calender.add(new Day("November", day, i + 1));
            day++;
            if(day > 7)
            {
                day = 1;
            }
        }
        
        for(int i = 0; i < DAYS_IN_DECEMBER; i++)
        {
            calender.add(new Day("December", day, i + 1));
            day++;
            if(day > 7)
            {
                day = 1;
            }
        }
        
        for(int i = 0; i < DAYS_IN_JANUARY; i++)
        {
            calender.add(new Day("January", day, i + 1));
            day++;
            if(day > 7)
            {
                day = 1;
            }
        }
        
        for(int i = 0; i < DAYS_IN_FEBRUARY; i++)
        {
            calender.add(new Day("February", day, i + 1));
            day++;
            if(day > 7)
            {
                day = 1;
            }
        }
        
        for(int i = 0; i < DAYS_IN_MARCH; i++)
        {
            calender.add(new Day("March", day, i + 1));
            day++;
            if(day > 7)
            {
                day = 1;
            }
        }
    }
}
