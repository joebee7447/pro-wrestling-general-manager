/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package generalmanager;

import java.util.List;

/**
 *
 * @author jtbum
 */
public class Show 
{
    private String showName;
    private String showPath;    //path to photo of show logo
    private List<Wrestler> wrestlers;   //the roster
    private int dayofWeek;      //day of week the show is scheduled
    private int numFans;        //number of weekly viewers
    private String nameWorld;
    private String nameInter;
    private String nameTag;
    private String nameWomens;
    private String pathWorld;       //indicates the path to the picture of the title
    private String pathInter;
    private String pathTag;
    private String pathWomens;
    private int money;
    private boolean isPlayer; //determines if player is playing as this brand
    private List<Email> emails; //list of emails
    
    public Show()
    {
        
    }
    
    public Show(String showName, String showPath, int dayofWeek, boolean isPlayer)
    {
        this.showName = showName;
        this.showPath = showPath;
        this.dayofWeek = dayofWeek;
        this.isPlayer = isPlayer;
    }
    
    public void addWrestler(Wrestler w)
    {
        wrestlers.add(w);
    }
    
    public void removeWrestler(Wrestler w)
    {
        wrestlers.remove(w);
    }
}
