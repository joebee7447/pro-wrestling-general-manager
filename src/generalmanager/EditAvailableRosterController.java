package generalmanager;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class EditAvailableRosterController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

     @FXML
    private GridPane wrestlerList;
     
     @FXML
    private Button backButton;
     
     @FXML
    void goBack(ActionEvent event) throws IOException {
        Parent menuParent = FXMLLoader.load(getClass().getResource("TitleMenu.fxml"));
        Scene menuScene = new Scene(menuParent);
        
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        window.setScene(menuScene);
        window.show();
    }
    
    @FXML
    void addWrestler(ActionEvent event) throws IOException {
        
    }
    
    @FXML
    void initialize() {
        

    }
}
