/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package generalmanager;

import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author jtbum
 */
public class GeneralManager extends Application{

    
    
    public static void main(String[] args) 
    {
        launch(args);
        
        // TODO code application logic here
    }
    
    public void start(Stage stage) throws Exception 
    {		
		Parent root = FXMLLoader.load(getClass().getResource("TitleScreen.fxml"));
                //Parent root = FXMLLoader.load(getClass().getResource("EditAvailableRoster.fxml"));
		
                Scene scene = new Scene(root);
		stage.setTitle("Wrestling General Manager");
		stage.setScene(scene);
		stage.show();
                
    }
    
}
